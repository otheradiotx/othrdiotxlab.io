---
title: Insect Records
layout: page
category: shows
time: 10 PM - 11 PM CST
---
Insect Records is a small record label based in Austin, Tx. They have been putting out quality vinyl, cassette and flexi disc releases since 2008. Label head Butcher Bear plays drums in the band Black Mercy and is cofounder of the influential electronic producer event Exploded Drawing. Insect Radio is a series of mixes by some of the label’s artists and mentors.
