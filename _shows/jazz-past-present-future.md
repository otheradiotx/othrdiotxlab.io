---
title: Jazz Past Present Future
layout: page
category: shows
time: 5 PM - 6 PM CST
---

Jazz Past Present Future draws music from around the globe in an effort to showcase the best in jazz. The music of the past acts as a guide to the music of today and upcoming artists looking to make their visions of jazz a reality. Jazz PPF is hosted by Austin Jazz DJ and record collector, Violet Sound. 
