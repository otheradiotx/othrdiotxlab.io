---
title: Feedback Alliance
layout: page
category: shows
time: 2 AM - 3 AM CST
---
Feedback Alliance is a loose federation of DJs, musicians, and producers from Austin, TX. It began as a monthly beat challenge to push the boundaries and became a community support new producers in Austin. These mixes span 5 years of different beat challenges.  
