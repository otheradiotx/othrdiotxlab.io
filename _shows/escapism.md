---
title: Escapism
layout: page
category: shows
time: 8PM - 9PM CST
---
ESCAPISM focuses on the connective threads that makes seemingly completely different genres of music blend together so well.  Your host, Chief and TheDoomsdayDevice (a DJ with 2 decades of experience), guides you through the experience song by song.
