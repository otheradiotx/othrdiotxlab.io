---
title: Epicurean Sound System
layout: page
category: shows
time: 8PM - 9PM CST
---

Epicurean soundsytem is the sonic extension of truth via music -- the music journalism efforts of floridian turned wholehearted austinite Jared Buchsbaum. It represents a progressive voyage deep into the groovy worlds of funk, soul, beats, and jazz, and everything their influence has touched worldwide. Expect eclectic soundscapes unrestricted by genre limitations, future-oriented with the past in tow, as he freely dabbles within the best of all possible worlds.
