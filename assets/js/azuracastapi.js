var request = new XMLHttpRequest();

function azuracastAPI() {
  // request.send();

  // Begin accessing JSON data here
  var response = JSON.parse(this.response);

  if (request.status >= 200 && request.status < 400) {
    // console.log(document.getElementById('np_track_name').innerHTML)
    var current_dj = response.now_playing.song.artist;
    var track_description = response.now_playing.song.lyrics;
    var track_name = response.now_playing.song.title;
    var is_live = response.live.is_live;
    if (is_live == true) {
      var artwork = './assets/img/live-broadcast.jpg';
      // console.log('The content is currently live');
    }
    else {
      var artwork = response.now_playing.song.art;
    }
    if ( artwork == '' ) {
      document.getElementById('track_artwork').style = 'background: linear-gradient(rgba(0, 0, 0, 0.40), rgba(0, 0, 0, 0.40)), url(./assets/img/default.jpg); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; background-attachment: scroll;';
      // console.log('yeeeeeeeh no artwork!');
    }
    else {
      document.getElementById('track_artwork').style = 'background: linear-gradient(rgba(0, 0, 0, 0.40), rgba(0, 0, 0, 0.40)), url('+artwork+'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; background-attachment: scroll;';
    }
    document.getElementById('np_current_dj').innerHTML = current_dj;
    document.getElementById('header_np_dj').innerHTML = current_dj;
    document.getElementById('header_np_dj_small').innerHTML = current_dj;
    document.getElementById('track_description').innerHTML = track_description;
    document.getElementById('np_track_name').innerHTML = track_name;
    document.getElementById('header_np_track').innerHTML = track_name;
    document.getElementById('header_np_track_small').innerHTML = track_name;
    document.getElementById('np_title').innerHTML = 'Other Radio TX: ' + current_dj + ' - ' + track_name;
    // console.log("Current Artwork is: " + artwork);
    // console.log(current_dj);
    // console.log(track_description);
    // console.log(track_name);
    // console.log('This is the current value of the track_name: ' + document.getElementById('np_track_name').innerHTML);
  }
  else {
    // console.log('error! API gave invalid response');
  }
}
function updateAPI() {
  // console.log('sending a fucking request, i guess');
  request.open('GET', 'https://stream.vaporwav.in/api/nowplaying/1', true);
  request.send();
  request.onload = azuracastAPI;
}
window.onload = updateAPI;
setInterval(updateAPI, 10000);
