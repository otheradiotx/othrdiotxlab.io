---
title: Exotic Dances
layout: page
category: shows
time: 8PM - 9PM CST
---
Dan Gentile of Flying Turns has been DJing, collecting vinyl, and producing electronic music for over 15 years. Exotic Dances is a tour of every corner of the dance floor, a diverse selection of groove-based music that spans from the classic discotheques of New York City to the beaches of Brazil.
