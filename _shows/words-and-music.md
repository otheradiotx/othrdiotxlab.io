---
title: Words and Music
layout: page
category: shows
time: in Rotation
---

Words & Music explores the beginning, middle and current sonic output of producers, musicians, artists creating in Texas. What you’ll hear are selections that, once woven together, show a progression from humble beginnings to present day projects as well as what is on the horizon.
